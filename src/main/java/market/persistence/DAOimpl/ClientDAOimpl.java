package market.persistence.DAOimpl;

import market.domain.Client;
import market.persistence.DAO.ClientDAO;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Jenson Harvey on 03.10.2015.
 */

@Repository
public class ClientDAOimpl implements ClientDAO {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public void addClient(Client client) {
        sessionFactory.getCurrentSession().save(client);
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<Client> listClient() {
        return sessionFactory.getCurrentSession().createQuery("from Client ").list();
    }

    @Override
    public void removeClient(Integer id) {
        Client client = (Client) sessionFactory.getCurrentSession().load(Client.class, id);
        if (null != client) {
            sessionFactory.getCurrentSession().delete(client);
        }
    }
}
