package market.service;

import market.domain.Client;

import java.util.List;

/**
 * Created by Jenson Harvey on 03.10.2015.
 */
public interface ClientService {

    public void addClient(Client client);

    public List<Client> listClient();

    public void removeClient(Integer id);
}
