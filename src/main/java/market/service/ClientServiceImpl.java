package market.service;

import market.domain.Client;
import market.persistence.DAO.ClientDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by Jenson Harvey on 03.10.2015.
 */
@Service
public class ClientServiceImpl {
    @Autowired
    private ClientDAO clientDAO;

    @Transactional
    public void addClient(Client client) {
        clientDAO.addClient(client);
    }

    @Transactional
    public List<Client> listClient() {

        return clientDAO.listClient();
    }

    @Transactional
    public void removeClient(Integer id) {
        clientDAO.removeClient(id);
    }
}
